FROM centos:6.6 
MAINTAINER linuxu "linuxu2015@gmail.com"  
###生成lnmp的docker 镜像文件   
RUN yum install -y mysql-server mysql  wget 
ADD conf/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo
ADD conf/epel.repo /etc/yum.repos.d/epel.repo
ADD conf/nginx.repo /etc/yum.repos.d/nginx.repo
RUN yum install nginx php php-fpm -y 
RUN /etc/init.d/mysqld start &&\  
    mysql -e "grant all privileges on *.* to 'root'@'%' identified by '112613';"&&\  
    mysql -e "grant all privileges on *.* to 'root'@'localhost' identified by '112613';"&&\  
    mysql -u root -p112613 -e "show databases;"  
RUN mkdir /srv/www/demo -p
EXPOSE 3306 80   
ADD conf/nginx.conf /etc/nginx/nginx.conf
ADD conf/vhosts.conf /etc/nginx/conf.d/vhosts.conf
ADD conf/index.php /srv/www/demo/
ADD start.sh /root/
EXPOSE 3306
EXPOSE 80
CMD ["/root/start.sh"]
